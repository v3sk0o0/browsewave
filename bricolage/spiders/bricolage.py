import pdb
import scrapy

from scrapy.loader import ItemLoader
from scrapy_splash import SplashRequest
from scrapy.loader.processors import MapCompose, TakeFirst, Join, Compose
from w3lib.html import remove_tags

from bricolage.items import BricolageItem
from bricolage.items import replace_space, convert_to_dict

class BricolageSpider(scrapy.Spider):
    #identity
    name = "bricolage"
    counter = 0
    second_counter = 0

    start_urls = [
        'https://mr-bricolage.bg/bg/Instrumenti/Avto-i-veloaksesoari/Veloaksesoari/c/006008012'
    ]

    # Script, which simulates clicking over the "Наличност" tab in order the load the js
    script = """
        function main(splash, args)
            splash:go(args.url)
            splash:wait(4)
            splash:runjs('document.getElementsByClassName("js-pickup-in-store-button")[0].click()')
            splash:wait(4)
            return splash:html()
        end
        """

    #Response
    def parse(self, response):
        """
        Parsing 'start' page
        """
        for product in response.xpath("//div[@class='product']"):
            product_page = product.xpath(
                ".//div[@class='title']/a[@class='name']/@href").extract_first(
                )

            item = ItemLoader(item=BricolageItem(),
                              selector=product,
                              response=response)

            item.add_value(
                'product_name',
                product.xpath(
                    ".//div[@class='image']/a/img/@title").extract_first())
            item.add_value(
                'price',
                product.xpath(".//div[@class='price']/text()").extract_first())
            
            item.add_value(
                'image_urls',
                product.xpath(
                    ".//div[@class='image']/a/img/@src").extract_first())

            if product_page is not None:
                product_page_url = response.urljoin(product_page)
                request = SplashRequest(url=product_page_url,
                                        callback=self.parse_product_page,
                                        endpoint='execute',
                                        args={'lua_source': self.script})
                request.meta['item'] = item
                yield request

        next_page = response.xpath("//a[@class='fa fa-chevron-right']/@href").extract_first()

        if next_page is not None:
            next_page_link= response.urljoin(next_page)
            yield scrapy.Request(url=next_page_link, callback=self.parse)

    def parse_product_page(self, response):
        """ 
        Product page parsing
        """
        item = response.meta['item']
        table_data = response.xpath("//table[@class='table']/tbody/tr")

        characteristics = dict()
        validator = ItemLoader(response)

        item.add_value(
            'product_name',
            response.xpath(
                '//h1[@class="js-product-name"]/text()').extract_first())

        for row in table_data:

            assert len(row.xpath("./td")) == 2, "Invalid parsing of the document"

            characteristics.update(
                    validator.get_value(
                    row.xpath("./td").extract(), 
                    MapCompose(remove_tags, str.rstrip, str.lstrip, replace_space),
                    Compose(convert_to_dict))
            )
           
        total_store_availability = {}

        for shop in response.xpath(
                "//ul[@class='pickup-store-list js-pickup-store-list']/li/label"
        ):

            shop_location = validator.get_value(
                shop.xpath("span[@class='pickup-store-info']/span").extract(),
                Compose(Join(", "), remove_tags))

            store_availability = validator.get_value(
                shop.xpath("span[@class='store-availability']/span")
                [0].extract(), Compose(remove_tags, replace_space))

            total_store_availability[shop_location] = store_availability

        item.add_value('total_store_availability', total_store_availability)
        item.add_value('url', response.url)
        item.add_value('characteristics', characteristics)
        result = item.load_item()
        yield result