# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html
import re
import scrapy

from scrapy.loader.processors import MapCompose, TakeFirst, Join, Compose
from w3lib.html import remove_tags

def price_cleanup(price):
    
    return  float(re.sub(r'[^\d,]+', "", price).replace(",", "."))

def replace_space(str_text):

    return str_text.replace(u"\xa0", ' ')


convert_to_dict = lambda x : {key: value for (key, value) in zip(x[::2], x[1::2])}

class BricolageItem(scrapy.Item):
    # define the fields for your item here like:

    product_name= scrapy.Field(
        input_processor=  MapCompose(str.strip, str.rstrip, replace_space),
        output_processor= TakeFirst()
    )
    
    price = scrapy.Field(
            input_processor=  MapCompose(str.strip, str.rstrip, price_cleanup),
            output_processor= TakeFirst()
    )

    total_store_availability = scrapy.Field(
        output_processor=TakeFirst()
        ) 
    characteristics = scrapy.Field(
        output_processor=TakeFirst()
    )
    url = scrapy.Field(
        output_processor=TakeFirst()
    )
    test =  scrapy.Field(
        input_processor= Compose(Join(", "), remove_tags),
        output_processor=Join(", ")
     )

    images = scrapy.Field(),
    image_urls = scrapy.Field(default=[])

